<%-- 
    Document   : login
    Created on : Nov 30, 2018, 11:43:22 AM
    Author     : neron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="includes/head.jsp" %>
    <body>
        <%@include file="includes/header.jsp" %>
        <main class="h-75 d-flex align-items-center">
            <div class="w-50 mx-auto form-container p-4">
                <center>
                        <p class="w-50 text-center form-title">LOGIN</p>
                </center>
                <form action="controllers/loginProceed.jsp" method="post">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email"
                                aria-describedby="emailHelp" placeholder="Email"
                                name="email" required="true">
                        </div>
                        <div class="form-group">
                            <label for="password">Mot de passe</label>
                            <input type="password" class="form-control"
                                id="password" placeholder="Mot de passe"
                                name="password" required="true">
                        </div>
                    <div class="d-flex pt-2">
                        <button type="submit" class="btn ml-auto">Valider</button>
                    </div>    
                </form>
            </div>
        </main>
    </body>
    <%@include file="assets/styleForm.jsp" %>
</html>
