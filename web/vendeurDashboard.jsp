<%-- 
    Document   : vendeurDashboard.
    Created on : Nov 30, 2018, 2:41:40 PM
    Author     : neron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"></meta>
        <title>Menu | PHONYSHOP</title>
        <link rel="stylesheet" href="styles/style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </head>
    <body>
        <!--NavBar Menu -->
    <nav class="navbar navbar-expand-lg navbar navbar-dark bg-dark">
      <a class="navbar-brand" href="#">PHONYSHOP</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown">
            <a class="nav-elmt nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Mettre En Vente
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
              <a class="dropdown-item" href="#">Ajouter Portable</a>
              <a class="dropdown-item" href="#">Modifier Portable</a>
              <a class="dropdown-item" href="#">Consulter Portable</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Gérer mes Ventes</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link active nav-elmt" href="#">Traiter Commande</a>
          </li>
          <li class="nav-item">
            <a class="nav-link active nav-elmt" href="#">Choisir Livreur</a>
          </li>
        </ul>
        <div >
            <ul class="navbar nav-left">
                <div class="nav-item dropdown">
                    <a class="nav-elmt nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Paramètres
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#">Profil</a>
                      <a class="dropdown-item" href="#">Notifications</a>
                    </div>
                </div>
            </ul>
        </div>
      </div>
    </nav>
        
        <!-- Search Form-->
    <div class="d-flex">
        <div class="w-50 mx-auto py-4">
            <form id="container-form" class="form-inline">
              <center>
              <input class="search form-control form-control-lg" type="search" placeholder="Search" aria-label="Search">
              <button type="submit" class="btn ml-auto btn-lg btn-primary">Search</button>
              </center>
            </form>
        </div>
    </div>    
    
    <!--Carousal images -->    
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="images/iphone-410324_1920.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="images/smartphone-1445489_1920.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="images/iphone-410311_1280.jpg" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    
    </body>
</html>
    
