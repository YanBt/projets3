<%-- 
    Document   : portable
    Created on : Dec 6, 2018, 6:18:31 PM
    Author     : neron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <%@include file="includes/head.jsp" %>
    <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>    
    <body>
        <%@include file="includes/header.jsp" %>
        <noscript>
            <strong>Nous sommes désolé mais Phonyshop ne marche pas correctement
            avec javascript désactivé. Veuillez l'autoriser sur cette page pour continuer.
            </strong>
        </noscript>
        <div id="app" class="h-100">
            <div class="side-nav">
                <%@include file="vue-app/views/sideNav.jsp" %>
            </div>

            <div class="search">
                <%@include file="vue-app/views/search.jsp" %>
            </div>

            <div class="panier">
                <%@include file="vue-app/views/panier.jsp" %>
            </div>

            <div class="results">
                <%@include file="vue-app/views/results.jsp" %>
            </div>
        </div>
    </body>
    <link rel="stylesheet" type="text/css" href="vue-app/app.css">
    <script src="vue-app/data.js"></script>
    <script src="vue-app/query.js"></script>
    <script src="vue-app/main.js"></script>
</html>
