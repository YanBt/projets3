<%-- 
    Document   : styleForm
    Created on : Nov 30, 2018, 12:30:28 PM
    Author     : neron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<style>
    body, html, main {
        height: 100%;
        background-color: aliceblue;
    }
    button {
        background-color: #141516 !important;
    }

    label {
        margin-left: 16px;
    }
    
    .form-title {
        font-size: 32px;
        font-weight: bold;
        background-color: #fff;
    }
    
    .form-container, button, input, .form-title {
        border-radius: 48px 48px 48px 48px !important;
        -moz-border-radius: 48px 48px 48px 48px !important; 
        -webkit-border-radius: 48px 48px 48px 48px !important;
    }

    .form-container {
        min-width: 320px;
        -webkit-box-shadow: 10px 10px 43px 13px rgba(20, 21, 22,0.43);
        -moz-box-shadow: 10px 10px 43px 13px rgba(20, 21, 22,0.43);
        box-shadow: 10px 10px 43px 13px rgba(20, 21, 22,0.43);
    }
</style>
