var main = new Vue({
	el:'#app',
	data: data,
	mounted () {
		this.loadOffres()
		// fetch('api/Offre')
		// 	.then(function(response) {
		// 		return response.json()
		// 	})
		// 	.then(function(OffresFetched) {
		// 		this.offres = OffresFetched
		// })
		// this.offres = query("api/Offre", {method: "GET"})
		// if (this.offres === true || this.offres ===false) {
		// 	this.offres = []
		// }
	},

	methods: {
		loadOffres: async function () {
			fetch('api/offre')
				.then(response => {
					return response.json()
					//this.offres = response.json()
				}).then (data => {

					this.offres = data
				})
		},

		ajouterAuPanier: function (index) {
			let offre = this.offres[index]
			if (offre.stock > 0) {
				offre.stock = offre.stock -1
				++offre.quantite
				let inPanier = false
				for (let elmt of this.panier.items) {
					if (elmt.id === offre.id) {
						inPanier = true
						break
					}
				}

				if (!inPanier) {
					offre.quantite = 1
					this.panier.items.push(offre)
					++this.panier.itemsCount
				}

			}

		},

		retirerDuPanier: function (index) {
			let panierElmt = this.panier.items[index]
			for (let offre of this.offres) {
				if (panierElmt.id === offre.id) {
					offre.stock += panierElmt.quantite
					offre.quantite = 0
					break
				}
			}
			//Modify data in server
			//
			--this.panier.itemsCount
			this.panier.items.splice(index, 1)
		},

		commander: function () {
			this.panier.items.forEach( (panierElmt) => {
				this.commande.tht += panierElmt.prix * panierElmt.quantite
				this.commande.lignesCommande.push(
						{
							offreID: panierElmt.id,
							quantiteCmd: panierElmt.quantite,
							totaleLigneCmd: panierElmt.quantite * panierElmt.prix
						}
					)
				}			  
			)

			console.log(JSON.stringify(this.commande))
			//Post to server
			query("api/commande", {
				method: "POST",
				body: JSON.stringify(this.commande)
			})
			$('#panier-modal').modal('toggle');

			alert("Commande effectuée avec succès!\nUn livreur vous contactera.\nTotal Hors taxe: " + this.commande.tht)

			this.panier.itemsCount = 0
			this.panier.items = []
		}
	}
})
