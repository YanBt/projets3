//const csrfToken = document.querySelector('[name="csrf-token"]').getAttribute('content')

function query (path, options) {
  const headers = new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/json'
    //'X-CSRF-TOKEN': csrfToken
  })

  options = Object.assign({
    credentials: 'same-origin',
    headers: headers,
    method: "GET"
  }, options)

  return fetch(path, options)
    .then(function (response) {
      const contentType = response.headers.get('content-type')

      if ((response.status !== 404 && contentType && contentType.includes('application/json'))) {
        return response.json()
      } else if (response.status === 204 && !contentType) {
        return true
      }
      return false
    })
    .catch(function () {
      return false
    })
}
