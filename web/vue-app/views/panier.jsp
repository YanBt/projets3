<%-- 
    Document   : panier
    Created on : Dec 8, 2018, 1:19:01 PM
    Author     : neron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<div class="d-flex justify-content-end">
	<div class="panier-show w-75 d-flex justify-content-end align-items-center"
	style="cursor: pointer;" data-toggle="modal" data-target="#panier-modal">
		<span class="mr-2">Panier</span>
		<img src="images/cart.png" style="width: 59px; height: 59px;">
		<h5><span class="badge badge-pill badge-info">{{ panier.itemsCount }}</span></h5>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="panier-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Panier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-flex flex-column">
      	<div class="panier-fields pb-2 row" style="background-color: #dee5ef;">
      		<span class="col-3">Portable</span>
      		<span class="col-3 text-center">Prix</span>
      		<span class="col-3 text-center">Quantite</span>
      		<span class="col-3 text-right">Operations</span>
      	</div>
        <div v-for="(portable, index) in panier.items" :key="portable.id" class="row">
        	<span class="col-3">{{ portable.marque }} {{ portable.modele }}</span>
        	<span class="col-3 text-center">{{ portable.prix }} Dhs</span>
        	<span class="col-3 text-center">{{ portable.quantite }}</span>
        	<div class="col-3 d-flex justify-content-end">
        		<a href="#" v-on:click="retirerDuPanier( index )"><img src="images/icons-trash.svg"></a>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Retour</button>
        <button type="button" class="btn btn-primary" v-on:click="commander">
	    	Commandez
    	</button>
      </div>
    </div>
  </div>
</div>