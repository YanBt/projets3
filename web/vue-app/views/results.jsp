<%-- 
    Document   : results
    Created on : Dec 8, 2018, 12:59:43 PM
    Author     : neron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<p class="mt-3 ">{{ portables.length }} résultats</p>
<div class="portables-list d-flex flex-wrap align-content-start align-items-center w-100 h-100 pl-0">
	<div class="portable col-4 pb-3 col-mx-12 pl-0 ml-0" :class="`portable-${portable.id}`"
	v-for="(portable, index) in portables" :key="portable.id">
		<div class="card h-50">
		  <img class="image card-img-top" :src="portable.url">
		  <div class="card-body">
		    <h5 class="card-title">{{ portable.marque }} {{ portable.modele}}</h5>
		    <div class="card-text d-flex">
		    	<p>Stock: {{ portable.stock }}</p>
		    	<p class="ml-auto">{{ portable.prix }} Dhs</p>
		    </div> 
		    
		    <a href="#" v-on:click = "ajouterAuPanier(index)" class="btn btn-outline-primary">Ajouter au panier</a>
		  </div>
		</div>
	</div>
</div>

<style scoped>
	.portable {
	  max-width: 200px;
	  font-size: 18px;
	}

	.image {
		max-height: 50px !important;
	}
</style>

