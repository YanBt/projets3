<%-- 
    Document   : sideNav
    Created on : Dec 8, 2018, 1:01:08 PM
    Author     : neron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<form>
	<h4>Filtres</h4>
	<div class="form-group">
		<label for="marques">Marque</label>
		<select class="form-control" id="marques">
			<option>Samsung</option>
			<option>Huawei</option>
			<option>Iphone</option>
			<option>Sony</option>
			<option>Nokia</option>
		</select>
	</div>
	<div class="form-group">
		<label for="model">Model</label>
		<select class="form-control" id="model">
			<option>S6</option>
			<option>S5</option>
			<option>8</option>
			<option>X</option>
			<option>Lumia</option>
		</select>
	</div>
  <button type="submit" class="btn btn-secondary">Filtrez</button>
</form>
