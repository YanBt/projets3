<%-- 
    Document   : signupProceed
    Created on : Nov 29, 2018, 10:58:54 PM
    Author     : neron
--%>

<%@page import="model.Vendeur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="../includes/head.jsp" %>
    <%@include file="../includes/sqlConnect.jsp" %>
    <sql:update dataSource = "${dbSource}" var = "result">
        <%= (new Vendeur(
                request.getParameter("email"),
                request.getParameter("nom"),
                request.getParameter("prenom"),
                request.getParameter("adresse"),
                request.getParameter("tel"),
                request.getParameter("password")
                )).save() %> 
    </sql:update>
    <body>
        <div class="row">
            <div class="col align-self-center">
                <center>
                    <p>Vous êtes maintenant un vendeur à Phonyshop</p><br>
                    <a href="../vendeurDashboard.jsp">Accédez à votre dashboard</a>
                </center>
                
            </div>
        </div>
    </body>
</html>
