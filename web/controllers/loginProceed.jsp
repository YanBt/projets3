<%-- 
    Document   : loginProceed
    Created on : Nov 30, 2018, 11:20:11 AM
    Author     : neron
--%>
<%@page import="model.Vendeur"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="../includes/head.jsp" %>
    <%@include file="../includes/sqlConnect.jsp" %>
    <sql:query dataSource = "${dbSource}" var = "result">
        <%= (new Vendeur()).all().where(
                    "emailVendeur",
                    request.getParameter("email")).where(
                    "pwdCryptVendeur",
                    request.getParameter("password")).get() %> 
    </sql:query>
    
    <body>
        <div class="row">
            <div class="col align-self-center">
                <c:choose>
                    <c:when test = "${result.rowCount > 0}">
                        <%
                        response.sendRedirect("/PhonyShop/vendeurDashboard.jsp");
                        %>
                    </c:when>
                    <c:otherwise>
                        <center>Echec d'authentification</center>
                    </c:otherwise>
                </c:choose>
                <br><center><a href="/PhonyShop/login.jsp">Retour au login</a></center>
            </div>
        </div>
    </body>
</html>
