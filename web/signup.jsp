<%-- 
    Document   : signup
    Created on : Nov 29, 2018, 8:20:00 PM
    Author     : neron
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="includes/head.jsp" %>
    <body>
        <main class="row">
            <div class="col d-flex align-self-center">
                <div class="w-50 mx-auto form-container p-4">
                    <center>
                        <p class="w-50 text-center form-title">INSCRIPTION VENDEUR</p>
                    </center>
                    
                    <form action="controllers/signupProceed.jsp" method="post">
                        <div class="form-group">
                            <label for="email">Email *</label>
                            <input type="email" class="form-control" id="email"
                                   aria-describedby="emailHelp" placeholder="Email"
                                   name="email" required="true">
                        </div>
                        <div class="form-group">
                            <label for="nom">Nom *</label>
                            <input type="text" class="form-control"
                                   id="nom" placeholder="Nom"
                                   name="nom" required="true">
                        </div>

                        <div class="form-group">
                            <label for="prenom">Prenom</label>
                            <input type="text" class="form-control"
                                   id="prenom" placeholder="Prénom"
                                   name="prenom">
                        </div>
                        <div class="form-group">
                            <label for="adresse">Adresse *</label>
                            <input type="text" class="form-control"
                                   id="adresse" placeholder="Adresse"
                                   name="adresse" required="true">
                        </div>
                        <div class="form-group">
                            <label for="tel">Téléphone *</label>
                            <input type="tel" class="form-control"
                                   id="tel" placeholder="Tel"
                                   name="tel" required="true">
                        </div>
                        <div class="form-group">
                            <label for="password">Mot de passe *</label>
                            <input type="password" class="form-control"
                                   id="password" placeholder="Mot de passe"
                                   name="password" required="true">
                        </div>
 
                        <div class="d-flex pt-2">
                            <button type="submit" class="btn ml-auto btn-lg btn-primary">Valider</button>
                        </div>    
                    </form>
                </div>
            </div>
            
        </main>
    </body>
    <%@include file="assets/styleForm.jsp" %>
</html>

