<%-- 
    Document   : signup
    Created on : Nov 29, 2018, 8:20:00 PM
    Author     : neron
--%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="../includes/head.jsp" %>
    <body>
        <%@include file="../includes/sqlConnect.jsp" %>
        <sql:query dataSource = "${dbSource}" var = "couleurs">
            SELECT * FROM COULEUR
        </sql:query>
        <sql:query dataSource = "${dbSource}" var = "portables">
            SELECT portableID, marquePortable, modelPortable FROM PORTABLE
        </sql:query>
        <main class="row">
            <div class="col d-flex align-self-center">
                <div class="w-50 mx-auto form-container p-4">
                    <center>
                        <p class="w-50 text-center form-title">METTRE EN VENTE UN PORTABLE</p>
                    </center>
                    
                    <form action="/api/portable" method="post">
                         <div class="form-group">
                            <label for="image">Image *</label>
                            <input type="file"
                                id="image" name="image"
                                accept="image/png, image/jpeg" multiple="false"
                                onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                            <br><img id="preview" width="150" />
                        </div>
                        
                        <div class="form-group">
                            <label for="portable">Portable *</label>
                            <select class="form-control tuned" name="portable" id="portable">
                                <c:forEach var="row" items="${portables.rows}">
                                    <option value="${row.portableID}"><c:out value="${row.marquePortable}"></c:out> <c:out value="${row.modelPortable}"></c:out></option>
                                </c:forEach>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="couleur">Couleur *</label>
                            <select class="form-control tuned" id="couleur">
                                <c:forEach var="row" items="${couleurs.rows}">
                                    <option value="${row.nomCouleur}"><c:out value="${row.nomCouleur}"></c:out></option>
                                </c:forEach>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="quantite">Quantite *</label>
                            <input type="number" class="form-control"
                                   id="quantite" placeholder="1"
                                   name="quantite" required="true">
                        </div>
                        
                        <div class="form-group">
                            <label for="prix">Prix Unitaire *</label>
                            <input type="text" class="form-control"
                                   id="prix" placeholder="400.00"
                                   name="prix" required="true">
                        </div>
 
                        <div class="d-flex pt-2">
                            <button type="submit" class="btn ml-auto btn-lg btn-primary">Valider</button>
                        </div>    
                    </form>
                </div>
            </div>
            
        </main>
    </body>
    <%@include file="../assets/styleForm.jsp" %>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.tuned').select2()
        })
    </script>
</html>