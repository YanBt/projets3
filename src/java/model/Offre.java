/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


import java.util.*;
import javax.json.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Akram
 */
public class Offre extends Model{

//Classe attributes;
    
    protected Integer id;
    protected Integer vendeurID;
    protected Integer quantite;
    protected Integer portableID;
    protected String couleur;
    protected Double prix;
    private String marque;
    private String modele;
    
//For new instance of object Portable;    
    
    public Offre (Integer vendeurID, Integer quantite, String couleur,
             Double prix){
        
        this.quantite= quantite;
        this.vendeurID = vendeurID;
        
        this.couleur= couleur; 
        
        this.prix= prix;
       
        //Set tablename, primaryKey
        super.initModel("¨Portable", "portableId");
        
    }
//Retreive the data from Database;        
    
    public Offre (Integer id, Integer vendeurID ,Integer quantite,
            String couleur, Double prix){
            
        this.id = id;
        this.vendeurID = vendeurID;
        this.quantite = quantite;
        
        this.couleur = couleur;
        
        this.prix = prix;
        
        super.initModel("Offre", "offreId");
        
    }
    
//Default constructor;
    
    public Offre(){

        super.initModel("Offre", "offreId");
    }
    
// Build Portables from resultSet
    
    public static ArrayList rsToList (ResultSet rs) {
        ArrayList<Offre> offres = new ArrayList();
        try {
            while (rs.next()) {
               Offre offre = new Offre ();
               offre.setId(rs.getInt("offreID"));
               offre.setPortableID(rs.getInt("portableID"));
               offre.setMarque(rs.getString("marquePortable"));
               offre.setModel(rs.getString("modelePortable"));
               offre.setPrix(rs.getDouble("prixPortable"));
               offre.setQuantite(rs.getInt("quantitePortable"));
               offre.setVendeurID(rs.getInt("vendeurID"));
               
               offres.add(offre);
            }          
        } catch (SQLException ex) {
            Logger.getLogger(Offre.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return offres;
    }
    
    public static JsonArray rsToJson (ResultSet rs) {
        ArrayList<Offre> offres = rsToList(rs);
        JsonArrayBuilder jsonList = Json.createArrayBuilder();
        for (Offre offre : offres) {
            jsonList.add(offre.toJson());
        }
        
        return jsonList.build();
    }
    
    public static String rsToString (ResultSet rs) {
        JsonArray jsonList = rsToJson(rs);
        String jsonString = "";
        try(Writer writer = new StringWriter()) {
            Json.createWriter(writer).write(jsonList);
            jsonString = writer.toString();
        } catch (IOException ex) {
            Logger.getLogger(Offre.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }
    
    

    //Seters and Getters;
    public Integer getPortableID() {
        return portableID;
    }

    public void setPortableID(Integer portableID) {
        this.portableID = portableID;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id){
        this.id= id;
    }

    public Integer getVendeurID(){
        return vendeurID;
    }

    public void setVendeurID(Integer vendeurID){
        this.vendeurID= vendeurID; 
    }

    public Integer getQuantite(){
        return quantite;
    }

    public  void setQuantite(Integer quantite){
        this.quantite= quantite;
    }

    public String getMarque(){
        return marque;
    }

    public void setMarque(String marque){
        this.marque=  marque;
    }

    public String getModel(){
        return modele;
    }

    public void setModel(String modele){
        this.modele= modele;
    }

    public String getColor(){
        return couleur;
    }

    public void setColor(String couleur){
        this.couleur=couleur;
    }
    
    public Double getPrix(){
        return prix;
    }

    public void setPrix(Double prix){
        this.prix= prix;
    }


    @Override
    HashMap<String, Object> getFields() {

        return new HashMap<String, Object>()
        {
            {//Colonum name with attribute classe;
                put("quantitePortable", quantite);
                put("vendeurID", vendeurID);
                put("portableID", portableID);
                put("couleur", couleur );
                put("prixPortable", prix);
            }
        };
    }

    public JsonObject toJson () {
        JsonObjectBuilder objectBuilder = Json.createObjectBuilder()
        .add("id", id)
        .add("portableID", portableID)
        .add("marque", marque)
        .add("modele", modele)
        .add("prix", prix)
        .add("stock", quantite)
        .add("vendeurID", vendeurID)
        .add("url", "https://i0.wp.com/www.dmvunlocked.com/wp-content/uploads/2015/09/Galaxy-S6.png?fit=833%2C870&ssl=1");

        return objectBuilder.build();

    }

    @Override
    public String toString() {

        JsonObject jsonObject = toJson();
        String jsonString = "";
        try(Writer writer = new StringWriter()) {
            Json.createWriter(writer).write(jsonObject);
            jsonString = writer.toString();
        } catch (IOException ex) {
            Logger.getLogger(Offre.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }


}
