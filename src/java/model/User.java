/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author neron
 */
public abstract class User extends Model{
    
    protected Integer id;
    protected String email;
    protected String nom;
    protected String prenom;
    protected String adresse;
    protected String tel;
    protected String passwordCrypt;
    
    //For new instance
    public User(String email, String nom, String prenom, String adresse, String tel, String passwordCrypt) {
        this.email = email;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.tel = tel;
        this.passwordCrypt = passwordCrypt;
    }
    
    //For instance retrieved from Bd
    public User(Integer id, String email, String nom, String prenom, String adresse, String tel) {
        this.id = id;
        this.email = email;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.tel = tel;
    }
    
    protected void initModel(String tableName, String...primaryKeys) {
        super.initModel(tableName, primaryKeys);
    }
    
    public User() {
        
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPasswordCrypt() {
        return passwordCrypt;
    }

    public void setPasswordCrypt(String passwordCrypt) {
        this.passwordCrypt = passwordCrypt;
    }
    
}
