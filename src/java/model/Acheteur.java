/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;

/**
 *
 * @author Akram
 */
public class Acheteur extends User {
    
    public Acheteur(String email, String nom, String prenom, String adresse, String tel, String passwordCrypt) {
        super(email, nom, prenom, adresse, tel, passwordCrypt);
        //Set tablename, primaryKey
        super.initModel("Acheteur", "acheteurID");
    }
    
    public Acheteur(Integer id, String email, String nom, String prenom, String adresse, String tel, String passwordCrypt) {
        super(email, nom, prenom, adresse, tel, passwordCrypt);
        super.initModel("Acheteur", "acheteurID");
    }
    
     public Acheteur() {
        super();
        super.initModel("Acheteur", "acheteurID");
    }
     
    @Override
    HashMap<String, Object> getFields() {
       
        return new HashMap<String, Object>()
        {
            {// Nom Colonne de la BD, attribut correspondant
                put("emailAcheteur", email);
                put("nomAcheteur", nom);
                put("prenomAcheteur", prenom);
                put("adresseAcheteur", adresse);
                put("telAcheteur", tel);
                put("pwdCryptAcheteur", passwordCrypt);
            }
        };
    }
}
