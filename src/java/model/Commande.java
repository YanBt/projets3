/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.json.JsonObject;

/**
 *
 * @author neron
 */
public class Commande extends Model{
    
    private Integer commandeID;
    private Integer acheteurID;
    private Double tht ;
    
    private ArrayList<LigneCommande> lignesCommande;

    public Commande(Integer commandeID, Integer acheteurID, Double tht) {
        this.commandeID = commandeID;
        this.acheteurID = acheteurID;
        this.tht = tht;
        this.lignesCommande = new ArrayList();
        
        super.initModel("Commande", "commandeID");
    }

    public Commande() {
        super();
        this.lignesCommande = new ArrayList();
        super.initModel("Commande", "commandeID");
    }

    public Commande(JsonObject commandeJson) {
        
        this.acheteurID = commandeJson.getInt("acheteurID");
        this.tht = commandeJson.getJsonNumber("tht").doubleValue();
        this.lignesCommande = new ArrayList();

        for (Object obj: commandeJson.getJsonArray("lignesCommande")) { 
            LigneCommande ligneCommande = new LigneCommande((JsonObject) obj);
            this.lignesCommande.add(ligneCommande);
        }
        
        super.initModel("Commande", "commandeID");
    }

    public Integer getCommandeID() {
        return commandeID;
    }

    public void setCommandeID(Integer commandeID) {
        this.commandeID = commandeID;
    }

    public Integer getacheteurID() {
        return acheteurID;
    }

    public void setacheteurID(Integer acheteurID) {
        this.acheteurID = acheteurID;
    }

    public Double getTht() {
        return tht;
    }

    public void setTht(Double tht) {
        this.tht = tht;
    }
    
    public boolean saveCommande() {
        saveData();
        ResultSet rs = select("max(commandeID)")
                        .where("acheteurID", this.acheteurID)
                        .getData();     
        try {
            rs.next();
            Offre offre = new Offre();
            this.commandeID = rs.getInt(1);
            System.out.println("1###############  "+  this.commandeID +" ################1");
            for (LigneCommande ligneCommande: this.lignesCommande) {
                ligneCommande.setCommandeID(this.commandeID);
                
                //Update offre
                ResultSet rsOffre = offre.findData(ligneCommande.getOffreID());
                rsOffre.next();
                offre.setId(rsOffre.getInt("offreID"));
                offre.setQuantite(rsOffre.getInt("quantitePortable") - ligneCommande.getQuantiteCmd());
                
                offre.updateData(offre.getId());
                ligneCommande.saveData();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Commande.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return true;
    }
    
    @Override
    HashMap<String, Object> getFields() {
       
        return new HashMap<String, Object>()
        {
            {// Nom Colonne de la BD, attribut correspondant
                put("commandeID", commandeID);
                put("acheteurID", acheteurID);
                put("tht", tht);
            }
        };
    }
    
    
}
