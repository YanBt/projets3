/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
/**
 *
 * @author Akram
 */
public class Livreur extends User{
    
    public Livreur (String email, String nom, String prenom, String adresse, String tel, String passwordCrypt) {
        super(email, nom, prenom, adresse, tel, passwordCrypt);
        //Set tablename, primaryKey
        super.initModel("Livreur", "livreurID");
    }
    
    public Livreur (Integer id, String email, String nom, String prenom, String adresse, String tel, String passwordCrypt) {
        super(email, nom, prenom, adresse, tel, passwordCrypt);
        super.initModel("Livreur", "livreurID");
    }
    
     public Livreur() {
        super();
        super.initModel("Livreur", "livreurID");

    }
     
    @Override
    HashMap<String, Object> getFields() {
       
        return new HashMap<String, Object>()
        {
            {// Nom Colonne de la BD, attribut correspondant
                put("emailLivreur", email);
                put("nomLivreur", nom);
                put("prenomLivreur", prenom);
                put("adresseLivreur", adresse);
                put("telLivreur", tel);
                put("pwdCryptLivreur", passwordCrypt);
            }
        };
    }
}
