/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author neron
 */
import generic.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Model {
    private String selectQuery;
    private String fromQuery;
    private String whereQuery;
    private String orderByQuery;
    private String limitQuery;
    private String joinQuery;
    private String groupByQuery;
    protected String tableName;
    private String[] primaryKeys;

    public Model() {
       
    }
    
    protected void initModel(String tableName, String...primaryKeys) {
        this.tableName = tableName;
        this.primaryKeys = primaryKeys;
        initQueries();
    }
    
    private void initQueries () {
        
        selectQuery = "";
        fromQuery = " FROM " + tableName;
        whereQuery = "";
        orderByQuery = "";
        limitQuery = "";
        joinQuery="";
        groupByQuery = "";
    }
    
    
	
    private static Connection connect() throws ClassNotFoundException {

        Class.forName("org.postgresql.Driver");

        try {

              Connection connection = DriverManager.getConnection(

                      "jdbc:postgresql://localhost:5432/phonyshop",

                      "neron", "postgres");

           return connection;

        } catch (SQLException e) {

            System.out.println("Connection failure.");

            e.printStackTrace();

            return null;

        }

    }
    
    public final Model all() {
        
        selectQuery = "SELECT *";
        return this;
    }
    
    public final String find (Object id) {
        
        selectQuery = "SELECT *";
        whereQuery = " WHERE ";
        whereQuery += primaryKeys[0] + "="+ SQLInjectionEscaper.format(id.toString());
        return get();
    }
    
    public final ResultSet findData (Object id) {
        
        selectQuery = "SELECT *";
        whereQuery = " WHERE ";
        whereQuery += primaryKeys[0] + "="+ SQLInjectionEscaper.format(id.toString());
        return getData();
    }
    
    public final Model select (String ... fields) {
        selectQuery = "SELECT ";
        if (fields.length > 0) {
            for (String field: fields) {
                selectQuery += field+ ", ";
            }
        
            selectQuery = selectQuery.substring(0, selectQuery.length() - 2);
        } else {
            selectQuery += "*";
        }
        return this;
    }
    
    public final Model where (Object ... clauses) {
        if(whereQuery == "")
            whereQuery = " WHERE ";
        else
            whereQuery+=" AND ";
        
        if (clauses.length == 2) {
            whereQuery += clauses[0]+ "="+ SQLInjectionEscaper.format(clauses[1
                    ].toString());
        } else if (clauses.length == 3 /*&& clauses.length % 2 == 0*/) {
            whereQuery += clauses[0].toString()+
                    clauses[1].toString()+
                    SQLInjectionEscaper.format(clauses[2].toString());
        } else {  
            whereQuery = "";
        }
        return this;
    }
    
    public final Model whereRaw (String clause, Object ... fillers) {
        whereQuery = " WHERE ";
        int i;
        for (Object filler: fillers) {
            i = clause.indexOf('?');
            if ( i == -1)
                break;
            clause = clause.substring(0, i-1) + 
                    filler +
                    clause.substring(i);
            return this;
        }
        clause.replace("?", "");
        
        whereQuery += SQLInjectionEscaper.format(clause);
        return this;
        
    }
    
    public final Model whereBetween (String field, Object inf, Object sup) {
        whereQuery = String.format(" WHERE %s BETWEEN %s and %s", field,
                inf, sup); 
        return this;
    }
    
    public final Model join (String ... tables) {
        for (String table: tables) {
            joinQuery += " NATURAL JOIN " + table;
        }
        
        return this;
    }
    
    public final Model limit (int n) {
        limitQuery = String.format(" LIMIT %d", n);
        return this;
    }
    
    public final Model take (int debut, int fin) {
        limitQuery = String.format(" LIMIT %d OFFSET %d",
                debut - fin, (fin -1)>0? fin - 1:0);
        return this;
    }
    
    public final Model page (int numeroPage, int nombreParPage) {
        
        return take((numeroPage -1)*nombreParPage, numeroPage * nombreParPage);
    }
    
    public final Model orderBy (String field, String mode) {
        orderByQuery = String.format(" ORDER BY %s %s", field, mode);
        return this;
    }
    
    public final Model sum (String field){
        
        if (selectQuery == "")
            selectQuery = "SELECT ";
        selectQuery +=  String.format("sum(%s)", field);
        return this;
    }
    
    public final Model count () {
        selectQuery = String.format("SELECT count(%s)", primaryKeys[0]);
        return this;
    }
    
    public final Model groupBy (String ... fields) {
        
        groupByQuery =  " GROUP BY ";
        
        for (String field: fields) {
            
            selectQuery += field+ ", ";
        }
        selectQuery = selectQuery.substring(0, selectQuery.length() - 2);
        return this;
    }
    
    //terminal method
    public final String get () {
        String query = selectQuery +
                    fromQuery +
                    joinQuery +
                    whereQuery +
                    groupByQuery +
                    limitQuery +
                    orderByQuery;
        initQueries();
        return query;
    }
    
    public final ResultSet getData () { 
        try {
            Connection con = connect();
            if(con != null) {
                Statement statement = con.createStatement();
                ResultSet result = statement.executeQuery(
                    get()
                );
                
                return result;
            }
        }   catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    abstract HashMap<String, Object> getFields ();
    
    //terminal method        
    public final String save() {
        
        String insertQuery="INSERT INTO " + tableName + " (";
        String insertQuery2=" VALUES (";
        
        for ( Map.Entry field: getFields().entrySet()) {
           // System.out.println("###################--"+ field.getValue().getClass().getName()+"--##################");
            if((field.getValue() != null) && (field.getKey() != primaryKeys[0])) {
                insertQuery += field.getKey() +", ";
                insertQuery2 += SQLInjectionEscaper.format(field.getValue().toString()) + ", ";
            }
        }
        
        insertQuery = insertQuery.substring(0, insertQuery.length() - 2) +")";
        insertQuery2 = insertQuery2.substring(0, insertQuery2.length() - 2);
        
        return insertQuery + insertQuery2+ ")";
        
    }
    
    public final void saveData() {
        
        try {
            Connection con = connect();
            if(con != null) {
                Statement statement = con.createStatement();
                statement.executeUpdate(
                    save()
                );
                
                //return result;
            }
        }   catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //return null;
        
    }
    
    //terminal method
    public final String update(Object id) {
        
        String updateQuery="UPDATE " + tableName + " SET ";
               

        for ( Map.Entry field:getFields().entrySet()) {
            
            if(field.getValue() != null && field.getKey() != primaryKeys[0]) {
                updateQuery += field.getKey() + " = "+ SQLInjectionEscaper.format(
                    field.getValue().toString()) + ", ";
            }
        }
        
        updateQuery = updateQuery.substring(0, updateQuery.length() - 2);
        whereQuery = " WHERE ";
        whereQuery += primaryKeys[0] + "="+ SQLInjectionEscaper.format(id.toString());
        
        return updateQuery + whereQuery;
    }
    
    public final void updateData(Object id) {
        
        try {
            Connection con = connect();
            if(con != null) {
                Statement statement = con.createStatement();
                statement.executeUpdate(
                    update(id)
                );
                
                //return result;
            }
        }   catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //return null;
    }
    
    public final String delete () {
        
        String deleteQuery = "DELETE FROM " + tableName + whereQuery;
        initQueries();
        return deleteQuery;
    }
    
    public final void deleteData () {
        
        try {
            Connection con = connect();
            if(con != null) {
                Statement statement = con.createStatement();
                statement.executeUpdate(
                    delete()
                );
                
                //return result;
            }
        }   catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //return null;
    }
    
    public final String delete (Object id) {
        whereQuery = " WHERE ";
        whereQuery += primaryKeys[0] + "="+ SQLInjectionEscaper.format(id.toString());
        return delete();
    }
    
    public final void deleteData (Object id) {
        try {
            Connection con = connect();
            if(con != null) {
                Statement statement = con.createStatement();
                statement.executeUpdate(
                    delete(id)
                );
                
                //return result;
            }
        }   catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //return null;
    }
        
}
