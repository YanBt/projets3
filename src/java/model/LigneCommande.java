/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import javax.json.JsonObject;

/**
 *
 * @author neron
 */
public class LigneCommande extends Model{
    private Integer commandeID;
    private Integer offreID;
    private Integer quantiteCmd;
    private Double totaleLigneCmd;

    public LigneCommande(Integer commandeID, Integer offreID, Integer quantiteCmd, Double totaleLigneCmd) {
        this.commandeID = commandeID;
        this.offreID = offreID;
        this.quantiteCmd = quantiteCmd;
        this.totaleLigneCmd = totaleLigneCmd;
        
        super.initModel("LigneCmd", "");
    }

    public LigneCommande() {
        super();
        super.initModel("LigneCmd", "");   
    }

    public LigneCommande(JsonObject obj) {
        
        this.offreID = obj.getInt("offreID");
        this.quantiteCmd = obj.getInt("quantiteCmd");
        this.totaleLigneCmd = obj.getJsonNumber("totaleLigneCmd").doubleValue();
        
        super.initModel("LigneCmd", "");
    }

    public Integer getCommandeID() {
        return commandeID;
    }

    public void setCommandeID(Integer commandeID) {
        this.commandeID = commandeID;
    }

    public Integer getOffreID() {
        return offreID;
    }

    public void setOffreID(Integer offreID) {
        this.offreID = offreID;
    }

    public Integer getQuantiteCmd() {
        return quantiteCmd;
    }

    public void setQuantiteCmd(Integer quantiteCmd) {
        this.quantiteCmd = quantiteCmd;
    }

    public Double getTotaleLigneCmd() {
        return totaleLigneCmd;
    }

    public void setTotaleLigneCmd(Double totaleLigneCmd) {
        this.totaleLigneCmd = totaleLigneCmd;
    }
    
    @Override
    HashMap<String, Object> getFields() {
       
        return new HashMap<String, Object>()
        {
            {// Nom Colonne de la BD, attribut correspondant
                put("commandeID", commandeID);
                put("offreID", offreID);
                put("quantiteCmd", quantiteCmd);
                put("totaleLigneCmd", totaleLigneCmd);
            }
        };
    }
    
    
    
}
