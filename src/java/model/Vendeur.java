/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;

/**
 *
 * @author neron
 */
public class Vendeur extends User {

    public Vendeur(String email, String nom, String prenom, String adresse, String tel, String passwordCrypt) {
        super(email, nom, prenom, adresse, tel, passwordCrypt);
        //Set tablename, primaryKey
        super.initModel("Vendeur", "vendeurID");
    }
    
    public Vendeur(Integer id, String email, String nom, String prenom, String adresse, String tel, String passwordCrypt) {
        super(email, nom, prenom, adresse, tel, passwordCrypt);
        super.initModel("Vendeur", "vendeurID");
    }
    
    public Vendeur() {
        super();
        super.initModel("Vendeur", "vendeurID");
    }
    
    @Override
    HashMap<String, Object> getFields() {
       
        return new HashMap<String, Object>()
        {
            {// Nom Colonne de la BD, attribut correspondant
                put("emailVendeur", email);
                put("nomVendeur", nom);
                put("prenomVendeur", prenom);
                put("adresseVendeur", adresse);
                put("telVendeur", tel);
                put("pwdCryptVendeur", passwordCrypt);
            }
        };
    }
    
}
